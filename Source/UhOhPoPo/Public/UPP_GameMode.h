// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UPP_GameMode.generated.h"


/**
 * 
 */
UCLASS()
class UHOHPOPO_API AUPP_GameMode : public AGameModeBase
{
	GENERATED_BODY()

	public:
	UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "OnInitGame"))
	void OnInitGame();

	virtual void InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage);
};
