// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UhOhPoPoGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UHOHPOPO_API AUhOhPoPoGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
