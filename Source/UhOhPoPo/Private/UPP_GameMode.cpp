// Fill out your copyright notice in the Description page of Project Settings.


#include "UPP_GameMode.h"


void UHOHPOPO_API AUPP_GameMode::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
    Super::InitGame(MapName, Options, ErrorMessage);

    OnInitGame();
}
